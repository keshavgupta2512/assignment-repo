package com.assignmenttask

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.assignmenttask.utils.AppToast
import com.assignmenttask.utils.InternetConnection
import com.assignmenttask.utils.UtilMethods

abstract class BaseActivity : AppCompatActivity() {
    private lateinit var mActivity: AppCompatActivity;
    private var mProgressDialog: AlertDialog? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mActivity = this;
    }

    val isNetworkConnected: Boolean
        get() = InternetConnection.isInternetConnected(this);


    fun showProgress() {
        hideProgress()
        mProgressDialog = UtilMethods.showProgressBar(this, getString(R.string.please_wait))
        mProgressDialog!!.show()
    }

    fun hideProgress() {
        if (mProgressDialog != null && mProgressDialog!!.isShowing) {
            mProgressDialog!!.cancel()
        }
    }

    fun showToast(message: Int) {
        AppToast.showToast(this, getString(message), Toast.LENGTH_SHORT)
    }

    fun showToast(message: String?) {
        AppToast.showToast(this, message, Toast.LENGTH_SHORT)
    }
}