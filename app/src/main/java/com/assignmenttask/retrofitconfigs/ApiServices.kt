package com.assignmenttask.retrofitconfigs

import com.assignmenttask.beans.DeliveryItem
import retrofit2.Call
import retrofit2.http.GET

interface ApiServices {
    @GET("deliveries")
    fun getDeliveredItemsListApi(): Call<List<DeliveryItem>>;
}