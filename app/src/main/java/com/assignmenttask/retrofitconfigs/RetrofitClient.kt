package com.assignmenttask.retrofitconfigs

import com.assignmenttask.BuildConfig
import com.assignmenttask.utils.Constants
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

enum class RetrofitClient {
    instance;

    @Volatile
    private var sRetrofit: Retrofit? = null
    private val baseUrl = Constants.BASE_URL
    val apiService: ApiServices

    init {
        sRetrofit = initRetrofit(baseUrl)
        apiService = sRetrofit!!.create(ApiServices::class.java)
    }

    private fun initRetrofit(baseUrl: String): Retrofit {

        val okHttpBuilder = OkHttpClient.Builder()
        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            okHttpBuilder.addInterceptor(interceptor)
        }
        val builder = Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create())
        return builder
            .client(
                okHttpBuilder.connectTimeout(5, TimeUnit.MINUTES)
                    .readTimeout(5, TimeUnit.MINUTES).writeTimeout(5, TimeUnit.MINUTES).build()
            )
            //.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setLenient().create()))
            .build()
    }


    private fun createClient(): OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(5, TimeUnit.MINUTES)
            .writeTimeout(5, TimeUnit.MINUTES).build()

    }

    companion object {
        private val TAG = RetrofitClient::class.java.simpleName
    }
}