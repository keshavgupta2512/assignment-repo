package com.assignmenttask.utils

import android.content.Context
import android.view.Gravity
import android.widget.Toast

class AppToast {

    companion object {
        var toast: Toast? = null
        fun showToast(
            mContext: Context,
            st: String?,
            length: Int
        ) {
            try {
                toast!!.view?.isShown     // true if visible
                toast!!.setText(st)
            } catch (e: Exception) {         // invisible if exception
                toast = Toast.makeText(mContext, st, length)
                toast!!.show()
            }

            toast!!.setGravity(Gravity.CENTER, 0, 0)
            toast!!.show()  //finally display it
        }
    }
}