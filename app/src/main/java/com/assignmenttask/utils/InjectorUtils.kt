package com.assignmenttask.utils

import com.assignmenttask.services.GetDeliveryItemRepository
import com.assignmenttask.ui.MainActivityFactory

object InjectorUtils {

    fun mainActivityFactory(): MainActivityFactory {
        val getDeliveryItemRepository = GetDeliveryItemRepository()
        return MainActivityFactory(getDeliveryItemRepository)
    }
}