package com.assignmenttask.utils

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.assignmenttask.R

class UtilMethods {

    companion object {

        fun showProgressBar(context: Context?, text: String): AlertDialog {
            val builder = AlertDialog.Builder(context!!)
            val dialog = builder.create()
            val dialogLayout: View
            dialogLayout = LayoutInflater.from(context).inflate(R.layout.custom_progress_bar, null)
            val text1 = dialogLayout.findViewById<View>(R.id.loading_msg) as TextView
            text1.text = text
            dialog.setView(dialogLayout)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            return dialog
        }
    }
}