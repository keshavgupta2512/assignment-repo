package com.assignmenttask.utils

class Constants {
    companion object {
        const val DELIVERY_DATA = "delivery_data_obj";
        const val BASE_URL = "https://demo1354773.mockable.io/";
    }

}