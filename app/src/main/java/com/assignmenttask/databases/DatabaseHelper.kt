package com.assignmenttask.databases

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.security.crypto.MasterKey
import net.sqlcipher.database.SQLiteDatabase
import net.sqlcipher.database.SQLiteDatabase.getBytes
import net.sqlcipher.database.SupportFactory
import java.io.IOException
import java.security.GeneralSecurityException

@Database(
    entities = [DeliveryItemsET::class],
    version = DbTables.APP_DB_VERSION,
    exportSchema = false
)
abstract class DatabaseHelper : RoomDatabase() {


    abstract fun deliveryItemsDao(): DeliveryItemsDao;

    companion object {
        var dbInstance: DatabaseHelper? = null


        fun getDbInstance(context: Context): DatabaseHelper {
            if (dbInstance == null) {
                dbInstance = initDatabase(context);
            }
            return dbInstance!!
        }

        fun initDatabase(context: Context): DatabaseHelper {
            try {
                val masterKeyAlia: MasterKey =
                    MasterKey.Builder(context).setKeyScheme(MasterKey.KeyScheme.AES256_GCM).build()
                val masterKeyAlias: String = masterKeyAlia.toString()
                val passphrase: ByteArray =
                    getBytes(SQLiteDatabase.getChars(masterKeyAlias.toByteArray()))
                val factory = SupportFactory(passphrase)
                return Room.databaseBuilder(
                    context,
                    DatabaseHelper::class.java,
                    DbTables.APP_DATABASE
                ).allowMainThreadQueries().openHelperFactory(factory).build()
            } catch (e: GeneralSecurityException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return getDbInstance(context)
        }
    }

}