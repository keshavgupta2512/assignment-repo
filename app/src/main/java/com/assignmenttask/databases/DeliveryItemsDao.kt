package com.assignmenttask.databases

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface DeliveryItemsDao {
     @Query("SELECT * FROM "+DbTables.DELIVERY_ITEMS)
     suspend fun getAll(): List<DeliveryItemsET>

     @Insert
     suspend fun insertAll(users: List<DeliveryItemsET>)

    @Delete
     suspend fun delete(user: DeliveryItemsET)

    @Query("DELETE FROM " + DbTables.DELIVERY_ITEMS)
    suspend fun deleteAll()
}