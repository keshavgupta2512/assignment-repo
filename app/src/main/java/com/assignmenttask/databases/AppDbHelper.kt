package com.assignmenttask.databases

interface AppDbHelper {
    suspend fun getUsers(): List<DeliveryItemsET>

    suspend fun insertAll(users: List<DeliveryItemsET>)

    suspend fun deleteAll();
}