package com.assignmenttask.databases

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = DbTables.DELIVERY_ITEMS)
data class DeliveryItemsET (
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "description") var description: String?,
    @ColumnInfo(name = "imageUrl") var imageUrl: String?,
    @ColumnInfo(name = "lat") var lat: Double,
    @ColumnInfo(name = "lng") var lng: Double,
    @ColumnInfo(name = "address") var address: String?
){
    constructor(description: String?, imageUrl: String?,lat :Double,lng: Double, address: String?) : this(0, description, imageUrl,lat,lng,address)
}