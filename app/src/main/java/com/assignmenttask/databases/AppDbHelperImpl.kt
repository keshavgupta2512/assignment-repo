package com.assignmenttask.databases

class AppDbHelperImpl(private val databaseHelper: DatabaseHelper) : AppDbHelper{
    override suspend fun getUsers(): List<DeliveryItemsET> =databaseHelper.deliveryItemsDao().getAll()

    override suspend fun insertAll(users: List<DeliveryItemsET>) = databaseHelper.deliveryItemsDao().insertAll(users);
    override suspend fun deleteAll() =databaseHelper.deliveryItemsDao().deleteAll();
}