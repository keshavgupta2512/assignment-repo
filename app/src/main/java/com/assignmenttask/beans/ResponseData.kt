package com.assignmenttask.beans

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ResponseData<T>{
    @SerializedName("data")
    @Expose
    var data: T? = null
}