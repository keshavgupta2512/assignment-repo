package com.assignmenttask.beans

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DeliveryItem()  {
    @SerializedName("id")
    @Expose
    var id: Long = 0

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("imageUrl")
    @Expose
    var imageUrl: String? = null

    @SerializedName("location")
    @Expose
    var deliveryItemLocationInfo: DeliveryItemLocationInfo? = null

}