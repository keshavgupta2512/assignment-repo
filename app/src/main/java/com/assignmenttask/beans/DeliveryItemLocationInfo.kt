package com.assignmenttask.beans

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DeliveryItemLocationInfo() {
    @SerializedName("lat")
    @Expose
    var lat: Double = 0.0

    @SerializedName("lng")
    @Expose
    var lng: Double = 0.0

    @SerializedName("address")
    @Expose
    var address: String? = null


}