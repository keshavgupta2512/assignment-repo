package com.assignmenttask.ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.assignmenttask.beans.DeliveryItem
import com.assignmenttask.databases.AppDbHelper
import com.assignmenttask.databases.DeliveryItemsET
import com.assignmenttask.services.GetDeliveryItemRepository
import com.bumptech.glide.load.engine.Resource
import kotlinx.coroutines.launch
import java.util.ArrayList

class MainActivityViewModel(val getDeliveryItemRepository: GetDeliveryItemRepository) :
    AndroidViewModel(Application()) {
    private val listDeliveryItemsET = MutableLiveData<List<DeliveryItemsET>>()



    fun fetchDeliveryItems(): MutableLiveData<List<DeliveryItem>> {
        return getDeliveryItemRepository.getDeliveryItemsData();
    }

    fun deleteUsers(appDbHelper: AppDbHelper,itemsApis:List<DeliveryItem>){
        viewModelScope.launch {
            listDeliveryItemsET.value=ArrayList();
            try {
                val itemsInDB= appDbHelper.getUsers();
                if(itemsInDB.isEmpty()){
                    val usersToInsertInDB = mutableListOf<DeliveryItemsET>()

                    for (apiUser in itemsApis) {
                        val user = DeliveryItemsET(
                            apiUser.description,
                            apiUser.imageUrl,
                            apiUser.deliveryItemLocationInfo!!.lat,
                            apiUser.deliveryItemLocationInfo!!.lng,
                            apiUser.deliveryItemLocationInfo?.address
                        )
                        usersToInsertInDB.add(user)
                    }
                    appDbHelper.insertAll(usersToInsertInDB);
                     Log.d("Size", usersToInsertInDB.size.toString())
                    listDeliveryItemsET.postValue(usersToInsertInDB)
                }else {
                    listDeliveryItemsET.postValue(itemsInDB)
                }

            } catch (e: Exception) {
                Log.d("Exception",e.message+"");
            }
        }
    }

    fun getDeliveryItems(): MutableLiveData<List<DeliveryItemsET>> {
        return listDeliveryItemsET
    }

}