package com.assignmenttask.ui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.View
import android.widget.SearchView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.assignmenttask.BaseActivity
import com.assignmenttask.R
import com.assignmenttask.adapters.DeliveryItemsListAdapter
import com.assignmenttask.databases.*
import com.assignmenttask.listeners.OnItemClickListener
import com.assignmenttask.utils.AppToast
import com.assignmenttask.utils.Constants
import com.assignmenttask.utils.InjectorUtils
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), OnItemClickListener.OnItemClickCallback {
    private lateinit var mainActivityViewModel: MainActivityViewModel;
    private lateinit var  deliveryItemsAdapter:DeliveryItemsListAdapter;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        rlv_deliveries.layoutManager = LinearLayoutManager(this);
        val factory = InjectorUtils.mainActivityFactory();
        mainActivityViewModel =
            ViewModelProvider(this, factory).get(MainActivityViewModel::class.java)
        init()
        setUpLiveObservers();

    }

    private fun setUpLiveObservers() {
        mainActivityViewModel.getDeliveryItems().observe(this, Observer {
            initAdapter(it)
        })
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.search_menu,menu)
        val search = menu?.findItem(R.id.searchFragment)
        val searchView = search?.actionView as SearchView
        searchView.queryHint = "Search"
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {

                    var dataList: MutableList<DeliveryItemsET> = mainActivityViewModel.getDeliveryItems().value?.filter{ it.description!!.contains(newText)}!!
                        .toMutableList()

                    if (dataList.size.equals(0)) {
                        AppToast.showToast(this@MainActivity, "No item Found",Toast.LENGTH_LONG);
                    }

                    for (item in dataList) {
                        System.out.println("Searched Data: " + item.description)
                    }
                    deliveryItemsAdapter?.updateList(dataList);
                }
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)

    }

    fun init() {
        if (isNetworkConnected) {
            showProgress();
            mainActivityViewModel.fetchDeliveryItems().observe(this, Observer {
                hideProgress()
                if (!it.isNullOrEmpty()) {
                    val appDbHelper = AppDbHelperImpl(DatabaseHelper.getDbInstance(this));
                    mainActivityViewModel.deleteUsers(appDbHelper, it);
                } else {
                    hideProgress()
                    showToast("Some error occurred")
                }
            })
        }
        else{
            val appDbHelper = AppDbHelperImpl(DatabaseHelper.getDbInstance(this));
            mainActivityViewModel.deleteUsers(appDbHelper,ArrayList())
            showToast("Check Internet Connection. Try again")
        }
    }

    fun initAdapter(arrayList: List<DeliveryItemsET>/*arrayList: List<DeliveryItem>*/) {
         deliveryItemsAdapter = DeliveryItemsListAdapter(arrayList, this, this)
        rlv_deliveries!!.adapter = deliveryItemsAdapter
    }

    override fun onItemClicked(view: View, position: Int, item: DeliveryItemsET) {
        val intent = Intent(this, MapsActivity::class.java);
        var bundle=Bundle();
        bundle.putString("IMAGEURL",item.imageUrl);
        bundle.putString("DESCRIPTION",item.description);
        bundle.putString("ADDRESS",item.address);
        bundle.putDouble("LAT",item.lat);
        bundle.putDouble("LNG",item.lng);
        intent.putExtra(Constants.DELIVERY_DATA,bundle);
        startActivity(intent)
    }
}



