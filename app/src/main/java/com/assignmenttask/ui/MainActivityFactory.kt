package com.assignmenttask.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.assignmenttask.services.GetDeliveryItemRepository

class MainActivityFactory(val getDeliveryItemRepository: GetDeliveryItemRepository) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MainActivityViewModel(getDeliveryItemRepository) as T
    }
}