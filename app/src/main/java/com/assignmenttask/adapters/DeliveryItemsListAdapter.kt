package com.assignmenttask.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.assignmenttask.R
import com.assignmenttask.databases.DeliveryItemsET
import com.assignmenttask.listeners.OnItemClickListener
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.textview.MaterialTextView


class DeliveryItemsListAdapter(
    var items: List<DeliveryItemsET>,
    val context: Context,
    val onItemClickCallback: OnItemClickListener.OnItemClickCallback
) : RecyclerView.Adapter<DeliveryViewHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeliveryViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return DeliveryViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: DeliveryViewHolder, position: Int) {
        val item = items[position]
        holder.bind(item, context)
        holder.itemView.setOnClickListener(OnItemClickListener(position, item, onItemClickCallback))
    }

     fun updateList( filterdList:List<DeliveryItemsET>){
        this.items=filterdList;
         notifyDataSetChanged()
     }



}

class DeliveryViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.inflate_delivery_items, parent, false)) {
    var tv_item_description: MaterialTextView? = null
    var ivItemImage: ImageView

    init {
        tv_item_description = itemView.findViewById(R.id.tv_item_description)
        ivItemImage = itemView.findViewById(R.id.iv_item_image)
    }

    fun bind(item: DeliveryItemsET, context: Context) {
        tv_item_description?.text = item.description
        Glide.with(context).load(item.imageUrl).diskCacheStrategy(DiskCacheStrategy.DATA)
            .error(R.drawable.no_image)
            .into(ivItemImage);
    }


}


