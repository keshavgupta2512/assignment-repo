package com.assignmenttask.services

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.assignmenttask.beans.DeliveryItem
import com.assignmenttask.beans.ResponseData
import com.assignmenttask.retrofitconfigs.RetrofitClient
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.logging.Logger

class GetDeliveryItemRepository {

    fun getDeliveryItemsData(): MutableLiveData<List<DeliveryItem>> {
        val data = MutableLiveData<List<DeliveryItem>>()
        RetrofitClient.instance.apiService.getDeliveredItemsListApi()
            .enqueue(object : Callback<List<DeliveryItem>> {
                override fun onFailure(call: Call<List<DeliveryItem>>, t: Throwable) {
                    throwError(t)
                }

                override fun onResponse(
                    call: Call<List<DeliveryItem>>,
                    response: Response<List<DeliveryItem>>
                ) {
                    if (response.code() == 200) {
                        data.value = response.body()
                        Log.i(
                            GetDeliveryItemRepository::class.java.simpleName,
                            response.body().toString()
                        )
                    } else {
                        val mydata = Gson().fromJson(
                            response.errorBody()?.string(),
                            ResponseData::class.java
                        )
                        data.value = mydata as List<DeliveryItem>
                        Log.i(
                            GetDeliveryItemRepository::class.java.simpleName,
                            response.body().toString()
                        )
                    }
                }
            })

        return data
    }

    private fun throwError(throwable: Throwable?) {
        Log.e("tag", throwable.toString())

    }
}