package com.assignmenttask.listeners

import android.view.View
import com.assignmenttask.beans.DeliveryItem
import com.assignmenttask.databases.DeliveryItemsET

class OnItemClickListener(private val position: Int,private val deliveryItem: DeliveryItemsET,private val onItemClickCallback: OnItemClickCallback) : View.OnClickListener {

    override fun onClick(view: View) {
        onItemClickCallback.onItemClicked(view,position,deliveryItem)
    }

    interface OnItemClickCallback {
        fun onItemClicked(view: View, position: Int,deliveryItem: DeliveryItemsET)
    }
}